﻿#include <iostream>
using namespace std;

class Vektor
{
public:
	Vektor(int _x, int _y, int _z) : x(_x), y(_y), z(_z) 
	{

	}
	double length()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
private:
	int x;
	int y;
	int z;
};

int main()
{
	setlocale(LC_ALL, ("ru"));
	Vektor a(12, 34, 10);
	cout << "Длинна вектора: " << a.length();
	return 0;
}

